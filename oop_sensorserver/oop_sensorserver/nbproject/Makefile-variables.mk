#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=arduino-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug/arduino-Linux-x86
CND_ARTIFACT_NAME_Debug=oop_sensorserver
CND_ARTIFACT_PATH_Debug=dist/Debug/arduino-Linux-x86/oop_sensorserver
CND_PACKAGE_DIR_Debug=dist/Debug/arduino-Linux-x86/package
CND_PACKAGE_NAME_Debug=oopsensorserver.tar
CND_PACKAGE_PATH_Debug=dist/Debug/arduino-Linux-x86/package/oopsensorserver.tar
# Release configuration
CND_PLATFORM_Release=arduino-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release/arduino-Linux-x86
CND_ARTIFACT_NAME_Release=oop_sensorserver
CND_ARTIFACT_PATH_Release=dist/Release/arduino-Linux-x86/oop_sensorserver
CND_PACKAGE_DIR_Release=dist/Release/arduino-Linux-x86/package
CND_PACKAGE_NAME_Release=oopsensorserver.tar
CND_PACKAGE_PATH_Release=dist/Release/arduino-Linux-x86/package/oopsensorserver.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
