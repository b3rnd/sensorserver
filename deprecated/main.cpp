/*
 * File:   main.cpp
 * Author: bernd
 *
 * Created on February 4, 2014, 5:13 PM
 */

#include <Arduino.h>

//int main(int argc, char** argv) {
//
//    init();
//    setup();
//
//    for(;;)
//        loop();
//
//    return 0;
//}
/*

 This sketch provides an TCP interface for testing hardware (NFC,...)
 of i.e. Android devices.

 *******************  COMMMANDS *******************
 * NFC Emulation:
 Command: "0<WRITEABLE><NFC-UID><OPTIONAL_NDEF_BUFFER>"
 - WRITEABLE hexstring of size 1 ("0" is writeable, else not writeable)
 - <NFC-UID> hexstring of size 6 ( i.e. 123456 )
 - <OPTIONAL_NDEF_BUFFER>  = <SIZE_NDEF_MESSAGE><NDEF_MESSGAGE>
 - <SIZE_NDEF_MESSAGE> = hexstring of size 4 (i.e. 001c)
 - <NDEF_MESSAGE> ... message in hexstring
 - full command example: 00123456001cD101185503646576656C6F7065722E636174726F6261742E6F72672F

 Board: Arduino Mega 2560
 Shields: NFC Shield V2.0, Ethernet Shield

 Attention both shields use the sampe SPI CS Pin (10). Therefore hardware modifications or "jumper cables" are necessary.

 Setup (see setup.jpg):
 Top:    Ethernet Shield
 Middle: NFC Shield V2.0
 Bottom: Arduino Mega 2560

 Pin 10 of of both Shields are bend out (not connected to base board).
 Connect following pins with jumper wires:
 - Arduino   Pin 10 <--> EthernetShield Pin10
 - NFCShield Pin  9 <--> EthernetShield Pin 9

 Now we can use pin 10 for Ehternet Shield and pin 9 for NFCShield.
 */

#include "SPI.h"
#include "PN532_SPI.h"
#include "emulatetag.h"
#include <Ethernet.h>

#define NFC_SHIELD_AVAILABLE

enum serialCommandPrefix {
  NFC_EMULATE, VIBRATION_VALUES, LIGHT_VALUES, VIBRATION_CALIBRATE
};

#define COMMAND_NFC_EMULATION_STARTED  "STARTED_NFC_EMULATION"
#define COMMAND_NFC_EMULATION_TIMEDOUT "TIMEDOUT_NFC_EMULATION"
#define COMMAND_NFC_EMULATION_FINISHED "FINISHED_NFC_EMULATION"
#define NDEF_MODIFIED                  "NDEF_MODIFIED:"

// ******************* Settings  *******************

#define STREAM_DELAY 2 // arduino is otherwise faster than the stream
#define NFC_EMULATION_TIMEOUT 5000
#define SERIAL_BUFFER_SIZE 128

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,8,16);
//IPAddress ip(10,0,0,33);

#define TCP_PORT 6789

// **************************************************

const int lightSensorPin       = A10;//2; // the ligth sensor sets a digital signal on pin 2
const int vibrationSensorPin   = A11;//A1; // the piezo is connected to analog pin 1
const int measurementStatusPin = 39;//7; // indicates the status of the hardware; is on while client is connected

// The last VIBRATION_BUFFER_SIZE measured data points are stored in vibrationBuffer
#define VIBRATION_BUFFER_SIZE 2048
uint16_t vibrationBuffer[VIBRATION_BUFFER_SIZE];
uint32_t vibrationCounter;
uint32_t vibrationThreshold;
uint32_t vibrationMeasurementDuration = 1000; //ms
#define CYCLE_DELAY 2 //Slow down the cycle time in the client.connected() loop

unsigned long timeoutCounter = 0;
#define CONNECTION_TIMEOUT 10000

// **************************************************

#define PN532_CS 9
#define ETHNET_CS 10

PN532_SPI pn532spi(SPI, PN532_CS);
EmulateTag nfc(pn532spi);

EthernetServer server(TCP_PORT);
EthernetClient client;

uint8_t hexCharToByte(char c);
void spiSelect(int CS);
bool readTwoCharConvertToByte(uint8_t *resultingByte);
bool readCharConvertToByte(uint8_t *resultingByte);
void initVibrationMeasurement();
void commandLightValues();
void measureVibration();
void commandVibrationValues();
void commandNfcEmulate();

// **************************************************

void setup()
{
  // disable all SPI
  pinMode(PN532_CS,OUTPUT);
  pinMode(ETHNET_CS,OUTPUT);
  digitalWrite(PN532_CS,HIGH);
  digitalWrite(ETHNET_CS,HIGH);

  pinMode(lightSensorPin, INPUT); // declare the light sensor pin as a INPUT
  pinMode(measurementStatusPin, OUTPUT); // declare the status pin as a OUTPUT
  digitalWrite(measurementStatusPin,LOW);

  Serial.begin(9600);

  #ifdef NFC_SHIELD_AVAILABLE
  spiSelect(PN532_CS);
  nfc.init();
  #endif

  spiSelect(ETHNET_CS);
  Ethernet.begin(mac, ip);
  server.begin();

  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
  Serial.print("timeout set to [s]: ");
  Serial.println(CONNECTION_TIMEOUT/1000);
  initVibrationMeasurement();

  Serial.println(F("INITIALIZATION_FINISHED"));
}

void loop(){
  client = server.available();

  if(client){
    Serial.println("client connected");

    if(client.connected()) {
      digitalWrite(measurementStatusPin,HIGH);
      timeoutCounter = millis() + CONNECTION_TIMEOUT;
    }

    while(client.connected()) {
      uint8_t command;

      if (timeoutCounter < millis()) {
          Serial.println("ERROR: connection timeout");
          break;
      }

      if(readCharConvertToByte(&command)) {

        timeoutCounter = millis() + CONNECTION_TIMEOUT;

        switch(command){
        case NFC_EMULATE:
          #ifdef NFC_SHIELD_AVAILABLE
          commandNfcEmulate();
          #endif
          break;
        case VIBRATION_VALUES:
          measureVibration();
          commandVibrationValues();
          break;
        case LIGHT_VALUES:
          commandLightValues();
          break;
        case VIBRATION_CALIBRATE:
          initVibrationMeasurement();
          client.print("CALIBRATION DONE. THRESHOLD = ");
          client.println(vibrationThreshold);
          delay(STREAM_DELAY);
          break;
        default:
          client.print("ERROR: command not understood:");
          client.println(command);
        }

        // read remaining bytes i.e. '\n'
        while(client.read() != -1){
          delay(STREAM_DELAY);
        }
      }


    }
    delay(STREAM_DELAY);
    client.stop();
    Serial.println("client disconnected\n");

    digitalWrite(measurementStatusPin,LOW);
  }
}

void commandNfcEmulate(){
  client.println(COMMAND_NFC_EMULATION_STARTED);

  uint8_t tagwriteable;
  uint8_t uid[3];

  if(  !readCharConvertToByte(&tagwriteable)
    || !readTwoCharConvertToByte(uid)
    || !readTwoCharConvertToByte(&uid[1])
    || !readTwoCharConvertToByte(&uid[2])
    ){
    client.println("ERROR");
    delay(STREAM_DELAY);
    return;
  }

  uint8_t* ndef_file = nfc.getNdefFilePtr();
  uint8_t i = 0;
  for(; i < nfc.getNdefMaxLength(); i++){
    if(!readTwoCharConvertToByte(ndef_file+i))
      break;
  }
  uint16_t ndefSize = (ndef_file[0] << 4) + ndef_file[1];
  if(i == 0 || (i < ndefSize)){
    ndef_file[0] = 0;
    ndef_file[1] = 0;
  }

  nfc.setTagWriteable(tagwriteable == 0);
  nfc.setUid(uid);

  spiSelect(PN532_CS);

//  nfc.init();

  if(!nfc.emulate(NFC_EMULATION_TIMEOUT)){
    delay(1000); // delay is mandatory!
    spiSelect(ETHNET_CS);
    client.println(COMMAND_NFC_EMULATION_TIMEDOUT);
  }
  else {
    delay(1000); // delay is mandatory!
    spiSelect(ETHNET_CS);
    if(nfc.writeOccured()){
      uint8_t* tag_buf;
      uint16_t length;

      nfc.getContent(&tag_buf, &length);

      client.print(NDEF_MODIFIED);
      uint16_t i;
      for(i = 0; i < length; i++){
        if(tag_buf[i] < 0x10){
          client.print("0");
        }
        client.print(tag_buf[i], HEX);
      }
      client.print("\n");
    }

    client.println(COMMAND_NFC_EMULATION_FINISHED);
  }
  delay(STREAM_DELAY);
}

void commandLightValues(){

  //int lightValue = digitalRead(lightSensorPin);
    int lightValue = analogRead(lightSensorPin);
    Serial.print("lightValue = ");Serial.println(lightValue);
  Serial.print((lightValue > 500) ? '0' : '1');
  Serial.println("LIGHT_END");

  if (lightValue > 500) {
      client.println("0LIGHT_END");
  } else {
      client.println("1LIGHT_END");
  }
  delay(STREAM_DELAY);
}

// **************************************************
void measureVibration() {
    uint32_t stopMeasurmentTime = millis() + vibrationMeasurementDuration;
    while (millis() < stopMeasurmentTime) {
        // Read vibration sensor val and store the data point in
        vibrationBuffer[vibrationCounter++ % VIBRATION_BUFFER_SIZE] = analogRead(vibrationSensorPin);
    }
}
void commandVibrationValues(){
    int32_t diff = 0;
    uint32_t diff_sum = 0;
    for (int idx = 1; idx < VIBRATION_BUFFER_SIZE; idx++)
    {
        diff = vibrationBuffer[idx] - vibrationBuffer[idx - 1];
        if (diff < 0)
            diff = -diff;
        if (diff > 1023)
            diff = 1023;
        diff_sum += diff;
    }
    Serial.print("diff_sum = "); Serial.println(diff_sum);
    Serial.print((diff_sum > vibrationThreshold) ? '1' : '0');
    Serial.println("VIBRATION_END");
    client.print((diff_sum > vibrationThreshold) ? '1' : '0');
    client.println("VIBRATION_END");
    delay(STREAM_DELAY);
}


// **************************************************

void spiSelect(int CS) {

  if(CS == ETHNET_CS){
    digitalWrite(PN532_CS,HIGH);
    SPI.setBitOrder(MSBFIRST);
  }
  else if(CS == PN532_CS){
    digitalWrite(ETHNET_CS,HIGH);
    SPI.setBitOrder(LSBFIRST);
  }

  // enable the chip we want
  digitalWrite(CS,LOW);
}

bool readCharConvertToByte(uint8_t *resultingByte){
  if(!client.available())
    return 0;

  int tmp = client.read();
  if(tmp == -1){
    return 0;
  }

  *resultingByte = hexCharToByte((char)tmp);
  delay(STREAM_DELAY);

  return 1;
}

bool readTwoCharConvertToByte(uint8_t *resultingByte){
  uint8_t msb;
  uint8_t lsb;

  if(readCharConvertToByte(&msb) && readCharConvertToByte(&lsb)){
    *resultingByte = (msb << 4) + lsb;
    return 1;
  }
  return 0;
}

void initVibrationMeasurement()
{
    Serial.println("calibrating vibration sensor...");
    digitalWrite(measurementStatusPin, HIGH);
    int32_t diff = 0;
    uint32_t diff_sum = 0;
    uint32_t average = 0;
    vibrationBuffer[0] = analogRead(vibrationSensorPin);
    for (int i = 0; i < 8; i++)
    {
        for (int idx = 1; idx < VIBRATION_BUFFER_SIZE; idx++)
        {
            vibrationBuffer[idx] = analogRead(vibrationSensorPin);
            diff = vibrationBuffer[idx] - vibrationBuffer[idx - 1];
            if (diff < 0)
                diff = -diff;
            if (diff > 1023)
                diff = 1023;
            diff_sum += diff;
        }
        average += diff_sum / 8;
        diff = 0;
        Serial.print("diff_sum ");Serial.print(i); Serial.print(" = ");
        Serial.println(diff_sum);
        diff_sum = 0;
    }
    vibrationThreshold = 2 * average;
    digitalWrite(measurementStatusPin, LOW);
    Serial.print("...calibration done! threshold = ");
    Serial.println(vibrationThreshold);
}

uint8_t hexCharToByte(char c){
  if(c >= '0' && c <= '9')
    return c - '0';
  if(c >= 'a' && c <= 'f')
    return c - 'a' + 10;
  if(c >= 'A' && c <= 'F')
    return c - 'A' + 10;
  return 0;
}

