#include <iostream>

using namespace std;

typedef unsigned char uint8_t;
typedef unsigned int uint32_t;

//---------------------------------------------------------------------
bool hexStringToByteArray( char *str, 
                           uint32_t strlen,
                           uint8_t bytes[])
{
    if ( strlen % 2 )
    {
#ifdef DEBUG_SERIAL
        Serial.print("ERROR: invalid string length to convert: ");
        Serial.println(strlen);
#endif
        return true;
    }
    
    for ( uint32_t c = 0; c < (strlen / 2); c++ )
    {
        if ( str[2 * c] >= '0' && str[2 * c] <= '9' )
            bytes[c] += (str[2 * c] - '0') << 4;
        else if ( str[2 * c] >= 'a' && str[2 * c] <= 'f' )
            bytes[c] += (str[2 * c] - 'a' + 10) << 4;
        else if ( str[2 * c] >= 'A' && str[2 * c] <= 'F' )
            bytes[c] += (str[2 * c] - 'A' + 10) << 4;
        else
        {
#ifdef DEBUG_SERIAL
            Serial.print("ERROR: invalid hex string");
#endif
            return true;
        }
        
        if ( str[2 * c + 1] >= '0' && str[2 * c + 1] <= '9' )
            bytes[c] += str[2 * c + 1] - '0';
        else if ( str[2 * c + 1] >= 'a' && str[2 * c + 1] <= 'f' )
            bytes[c] += str[2 * c + 1] - 'a' + 10;
        else if ( str[2 * c + 1] >= 'A' && str[2 * c + 1] <= 'F' )
            bytes[c] += str[2 * c + 1] - 'A' + 10;
        else
        {
#ifdef DEBUG_SERIAL
            Serial.println("ERROR: invalid hex string");
#endif
            return true;
        }
    }
    return false;
}

//---------------------------------------------------------------------
int main ( int argc, char **argv )
{
  cout << "started" << endl;
  
  char *str = "abcdef";
  uint8_t bytes[] = {0, 0, 0};
  
  for (int i = 0; i < 3; i++)
    cout << " " << (int)bytes[i];
  cout << endl;
  
  if ( !hexStringToByteArray(str, 6, bytes ) )
  {
    cout << "success" << endl;
    for (int i = 0; i < 3; i++)
        cout << " " << (int)bytes[i];
    cout << endl;
  }
  else
  {
    cout << "failed" << endl;
  }
  
  cout << "finished" << endl;

  return 0;
}

