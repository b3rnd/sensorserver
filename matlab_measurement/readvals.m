clear all; close all;

vib1 = csvread('measurement1.csv');
vib2 = csvread('measurement2.csv');
vib3 = csvread('measurement3.csv');

vib4 = csvread('measurement4.csv');
vib5 = csvread('measurement5.csv');
vib6 = csvread('measurement6.csv');

vibvalues = [vib1 vib2 vib3 vib4 vib5 vib6];

maxvalues = max(vibvalues, [], 1);
minvalues = min(vibvalues, [], 1);

x = 0:2047;
figure;
plot(x, vib6 - mean(vib6), 'b');
hold on;
plot(x, vib1 - mean(vib1), 'r');
